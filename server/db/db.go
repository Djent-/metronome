package DB

import (
	"log"
	"database/sql"
	"errors"
	_ "github.com/mattn/go-sqlite3"
)

type Account struct {
	Username *string
	PasswordHash *string
	Email *string
	Untitled *int
}

type Song struct {
	Id *int
	Title *string
	BPM *int
	Upper *int
	Lower *int
	Advanced *string
	Duration *int
	Owner *Account
}

type Playlist struct {
	Id *int
	Title *string
	Owner *Account
	Songs *[]Song
}

type Token struct {
	Value *string
	Owner *string
}

var (
	// Public variables
	Database *sql.DB
	ErrNoAccount = errors.New("No corresponding account found.")
	ErrNoSession = errors.New("No corresponding session found.")
	ErrNoSong = errors.New("No corresponding song found.")
	ErrNoPlaylist = errors.New("No corresponding playlist found.")
	
	// Private variables
	sessionOwner = `SELECT users.username, users.passwordHash, users.email, users.untitled
					FROM users, tokens 
					WHERE users.username=tokens.owner 
					AND tokens.value=?`
	insertToken = `INSERT INTO tokens
					VALUES (?,?)`
	deleteToken = `DELETE FROM tokens
					WHERE tokens.value=?`
	insertUser = `INSERT INTO users
					VALUES (?,?,?,?)`
	insertPlaylist = `INSERT INTO playlists
					VALUES (NULL,?,?)`
	insertSong = `INSERT INTO songs
					VALUES (NULL,?,?,?,?,?,?,?)`
	updateSong = `UPDATE songs
					SET title=?,
						bpm=?,
						upper=?,
						lower=?,
						advanced=?,
						duration=?
					WHERE id=?`
	queryAccount = `SELECT username, passwordHash, email, untitled 
					FROM users WHERE username=?`
	queryEmail = `SELECT username, passwordHash, email 
					FROM users WHERE email=?`
	requestSong = `SELECT id, title, bpm, upper, lower, advanced, duration, owner
					FROM songs
					WHERE id=?`
	querySongs = `SELECT id, title, bpm, upper, lower, advanced, duration, owner
					FROM songs
					WHERE owner=?`
	requestPlaylist = `SELECT id, title, owner
					FROM playlists
					WHERE id=?`
	queryPlaylists = `SELECT id, title, owner
					FROM playlists
					WHERE owner=?`
	deleteSong = `DELETE FROM songs
					WHERE id=?`
	deletePlaylist = `DELETE FROM playlists
					WHERE id=?;
					DELETE FROM contains
					WHERE pid=?`
	deleteSongAssc = `DELETE FROM contains
					WHERE sid=?`
	deletePlaylistAssc = `DELETE FROM contains
					WHERE pid=?`
	queryPlaylistAssc = `SELECT sid FROM contains
					WHERE pid=?`
	deleteUser = `DELETE FROM users
					WHERE username=?`
	updateUser = `UPDATE users
					SET passwordHash=?
					WHERE username=?`
	updateUntitled = `UPDATE users
					SET untitled=?
					WHERE username=?`
	addPlaylistContains = `INSERT INTO contains
							VALUES (?,?)`
	updatePlaylistTitle = `UPDATE playlists
							SET title=?
							WHERE id=?`
)

// Opens and returns a new database handle
// Called by main() in server.go.
func DatabaseSetup() *sql.DB {
	log.Println("Setting up database.")
	db, err := sql.Open("sqlite3", "server.db")
	if err != nil {
		panic(err)
	}
	return db
}

func RequestAccount(username string) (*Account, error) {
	a := &Account{}
	err := Database.QueryRow(queryAccount, username).Scan(&a.Username, &a.PasswordHash, &a.Email, &a.Untitled)
	if err != nil {
		return nil, ErrNoAccount
	}
	return a, nil
}

func UsernameInUse(username string) bool {
	a := &Account{}
	err := Database.QueryRow(queryAccount, username).Scan(&a.Username, &a.PasswordHash, &a.Email, &a.Untitled)
	if err != nil || a.Username == nil {
		return false
	}
	return true
}

func EmailInUse(email string) bool {
	a := &Account{}
	err := Database.QueryRow(queryEmail, email).Scan(&a.Username, &a.PasswordHash, &a.Email)
	if err != nil || a.Email == nil {
		return false
	}
	return true
}

func RequestSessionOwner(token string) (*Account, error) {
	a := &Account{}
	err := Database.QueryRow(sessionOwner, token).Scan(&a.Username, &a.PasswordHash, &a.Email, &a.Untitled)
	if err != nil || a.Username == nil {
		return nil, ErrNoSession
	}
	return a, nil
}

/*
Get the song corresponding to the given song ID.
Returns the song and nil or nil and ErrNoSong or ErrNoAccount.
*/
func RequestSong(id int) (*Song, error) {
	s := &Song{}
	var owner string
	err := Database.QueryRow(requestSong, id).Scan(
		&s.Id, &s.Title, &s.BPM, &s.Upper, &s.Lower, &s.Advanced, &s.Duration, &owner)
	if err != nil {
		return nil, ErrNoSong
	}
	a, err := RequestAccount(owner)
	if err != nil {
		log.Println("Orphaned song.")
		return nil, ErrNoAccount
	}
	s.Owner = a
	return s, nil
}

/*
Get the playlist corresponding to the given playlist ID.
Returns the playlist and nil or nil and ErrNoPlaylist or ErrNoAccount.
*/
func RequestPlaylist(id int) (*Playlist, error) {
	p := &Playlist{}
	var owner string
	err := Database.QueryRow(requestPlaylist, id).Scan(&p.Id, &p.Title, &owner)
	if err != nil {
		return nil, ErrNoPlaylist
	}
	a, err := RequestAccount(owner)
	if err != nil {
		log.Println("Orphaned playlist.")
		return nil, ErrNoAccount
	}
	p.Owner = a
	var songs []Song
	// Get list of songs in the playlist
	songRows, err := Database.Query(queryPlaylistAssc, *p.Id)
	if err != nil {
		panic(err)
	}
	defer songRows.Close()
	for songRows.Next() {
		var id *int
		if err := songRows.Scan(&id); err != nil {
			panic(err)
		}
		song, err := RequestSong(*id)
		if err != nil {
			return nil, err
		}
		songs = append(songs, *song)
	}
	if err := songRows.Err(); err != nil {
		panic(err)
	}
	log.Println("p.Songs in RequestPlaylist: ")
	log.Println(p.Songs)
	p.Songs = &songs
	return p, nil
}

func (t *Token) Store() {
	log.Println("Storing new session token: " + *t.Value)
	tx, err := Database.Begin()
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	statement, err := Database.Prepare(insertToken)
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	_, err = tx.Stmt(statement).Exec(t.Value, t.Owner)
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
}

func (t *Token) Remove() {
	log.Println("Removing session token: " + *t.Value)
	tx, err := Database.Begin()
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	statement, err := Database.Prepare(deleteToken)
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	_, err = tx.Stmt(statement).Exec(t.Value)
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
}

func (a *Account) Store() {
	log.Println("Storing new user: " + *a.Username)
	tx, err := Database.Begin()
	if err != nil {
		panic(err)
	}
	statement, err := Database.Prepare(insertUser)
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	_, err = tx.Stmt(statement).Exec(a.Username, a.PasswordHash, a.Email, a.Untitled)
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
}

func (a *Account) Remove() {
	log.Println("Deleting user: " + *a.Username)
	tx, err := Database.Begin()
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	statement, err := Database.Prepare(deleteUser)
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	_, err = tx.Stmt(statement).Exec(a.Username)
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
}

func (a *Account) Update() {
	log.Println("Updating user: " + *a.Username)
	tx, err := Database.Begin()
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	statement, err := Database.Prepare(updateUser)
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	_, err = tx.Stmt(statement).Exec(a.PasswordHash, a.Username)
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
}

func (s *Song) Store() {
	log.Println("Storing new song: " + *s.Title)
	tx, err := Database.Begin()
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	statement, err := Database.Prepare(insertSong)
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	_, err = tx.Stmt(statement).Exec(s.Title, s.BPM, s.Upper, s.Lower, s.Advanced, s.Duration, s.Owner.Username)
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
}

func (s *Song) Update() {
	log.Println("Updating song: " + *s.Title)
	tx, err := Database.Begin()
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	statement, err := Database.Prepare(updateSong)
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	_, err = tx.Stmt(statement).Exec(s.Title, s.BPM, s.Upper, s.Lower, s.Advanced, s.Duration, s.Id)
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
}

func (s *Song) Remove() {
	log.Println("Removing song: " + *s.Title)
	tx, err := Database.Begin()
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	statement, err := Database.Prepare(deleteSong)
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	_, err = tx.Stmt(statement).Exec(s.Id)
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
}

func (p *Playlist) Store() {
	log.Println("Storing new playlist: " + *p.Title)
	tx, err := Database.Begin()
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	statement, err := Database.Prepare(insertPlaylist)
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	result, err := tx.Stmt(statement).Exec(p.Title, p.Owner.Username)
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	pid, err := result.LastInsertId()
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	// Add new connections for every song in this playlist
	songs, err := Database.Prepare(addPlaylistContains)
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	for _, song := range(*p.Songs) {
		_, err = tx.Stmt(songs).Exec(pid, song.Id)
		if err != nil {
		tx.Rollback()
		log.Println(err)
		}
	}
	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
}

func (p *Playlist) Update() {
	log.Println("Updating playlist: " + *p.Title)
	tx, err := Database.Begin()
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	
	// Update the playlist title
	title, err := Database.Prepare(updatePlaylistTitle)
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	_, err = tx.Stmt(title).Exec(p.Title, p.Id)
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	
	// Update contains table
	// Delete current playlist-song connections for this playlist Id
	contains, err := Database.Prepare(deletePlaylistAssc)
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	_, err = tx.Stmt(contains).Exec(p.Id)
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	
	// Add new connections for every song in this playlist
	songs, err := Database.Prepare(addPlaylistContains)
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	for _, song := range(*p.Songs) {
		_, err = tx.Stmt(songs).Exec(p.Id, song.Id)
		if err != nil {
			tx.Rollback()
			log.Println(err)
		}
	}
	
	// Commit the database transaction
	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
}

func (p *Playlist) Remove() {
	log.Println("Removing playlist: " + *p.Title)
	tx, err := Database.Begin()
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	statement, err := Database.Prepare(deletePlaylist)
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	log.Println("Executing delete playlist statement.")
	_, err = tx.Stmt(statement).Exec(p.Id)
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	statement, err = Database.Prepare(deletePlaylistAssc)
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	log.Println("Executing delete playlist associations statement.")
	_, err = tx.Stmt(statement).Exec(p.Id)
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
	err = tx.Commit()
	if err != nil {
		tx.Rollback()
		log.Println(err)
	}
}

// Returns a list of songs owned by the Account
func (a *Account) Songs() *[]Song {
	rows, err := Database.Query(querySongs, a.Username)
	if err != nil {
		panic(err)
	}
	defer rows.Close()
	var songs []Song
	for rows.Next() {
		var id *int
		var title *string
		var bpm *int
		var upper *int
		var lower *int
		var duration *int
		var advanced *string
		var owner *string
		// user RequestAccount(username)
		if err := rows.Scan(&id, &title, &bpm, &upper, &lower, &advanced, &duration, &owner); err != nil {
			panic(err)
		}
		acct, _ := RequestAccount(*owner)
		songs = append(songs, Song{id, title, bpm, upper, lower, advanced, duration, acct})
	}
	if err := rows.Err(); err != nil {
		panic(err)
	}
	return &songs
}

// Returns a list of playlists owned by the Account
func (a *Account) Playlists() *[]Playlist {
	playlistRows, err := Database.Query(queryPlaylists, a.Username)
	if err != nil {
		panic(err)
	}
	defer playlistRows.Close()
	var playlists []Playlist
	for playlistRows.Next() {
		var id *int
		var title *string
		var owner *string
		var songs []Song
		if err := playlistRows.Scan(&id, &title, &owner); err != nil {
			panic(err)
		}
		// Get list of songs in the playlist
		songRows, err := Database.Query(queryPlaylistAssc, *id)
		if err != nil {
			panic(err)
		}
		defer songRows.Close()
		for songRows.Next() {
			var sid *int
			if err := songRows.Scan(&sid); err != nil {
				panic(err)
			}
			song, err := RequestSong(*sid)
			if err != nil {
				panic(err)
			}
			songs = append(songs, *song)
		}
		if err := songRows.Err(); err != nil {
			panic(err)
		}
		acct, _ := RequestAccount(*owner)
		playlists = append(playlists, Playlist{id, title, acct, &songs})
	}
	if err := playlistRows.Err(); err != nil {
		panic(err)
	}
	return &playlists
}

// Increments and returns the number of untitled songs the user owns
func (a *Account) NewUntitled() *int {
	log.Println(a)
	if a.Untitled == nil {
		zero := 0
		a.Untitled = &zero
	}
	// Increment Untitled - it is an int reference
	untitled := *a.Untitled + 1
	a.Untitled = &untitled
	// Update Account Untitled in the database
	tx, err := Database.Begin()
	if err != nil {
		panic(err)
	}
	statement, err := Database.Prepare(updateUntitled)
	if err != nil {
		panic(err)
	}
	_, err = tx.Stmt(statement).Exec(a.Untitled, a.Username)
	if err != nil {
		panic(err)
	}
	err = tx.Commit()
	if err != nil {
		panic(err)
	}
	// Return the new value
	return a.Untitled
}