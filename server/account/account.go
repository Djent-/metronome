// account.go

package Account

import (
	"errors"
	"encoding/json"
	"net/url"
	"net/http"
	"html/template"
	"log"
	"fmt"
	"crypto/rand"
	"encoding/base64"
	"io"
	"time"
	"strconv"
	"strings"
	"../db"
	"golang.org/x/crypto/bcrypt"
)

var (
	ErrUsernameInUse = errors.New("Username is already in use.")
	ErrEmailInUse = errors.New("Email is already in use.")
	ErrPasswordMismatch = errors.New("Passwords do not match.")
	ErrInvalidCredentials = errors.New("Username or password do not match.")
	ErrNoForm = errors.New("No form during sign-up process.")
)

/*
These three structs hold the data passed to the corresponding templates.
Templates that only take the Account reference do not need structs.
*/
type PlaylistEditPage struct {
	Account *DB.Account
	Playlist *DB.Playlist
	SongList *[]DB.Song
	New *bool
}

type SongEditPage struct {
	Account *DB.Account
	Song *DB.Song
}

type AccountPage struct {
	Account *DB.Account
	Playlists *[]DB.Playlist
	Songs *[]DB.Song
}

type PlayPage struct {
	Account *DB.Account
	Playlist *DB.Playlist
	Song *DB.Song
}

/*
Struct used when saving a playlist in SavePlaylistHandler.
Passed as json from the client.
*/
type SavedPlaylist struct {
	New *bool
	Id *int
	Title *string
	Songs *[]int
}

/*
Handles GETs to /account.
Serves the account page populated with the user's songs and playlists.
*/
func AccountPageHandler(w http.ResponseWriter, req *http.Request) {
	// Check that the user is logged in
	authCookie, err := req.Cookie("auth")
	if (err != nil || !LoggedIn(authCookie)) {
		http.Redirect(w, req, "/", 403)
		return
	}
	
	// Populate the Account page with the user's data
	var page AccountPage
	page.Account, err = GetAccount(authCookie)
	if err != nil {
		http.Redirect(w, req, "/", 403)
		return
	}
	page.Playlists = page.Account.Playlists()
	page.Songs = page.Account.Songs()
	accountHTML, err := template.ParseFiles("templates/account.tpl")
	if err != nil {
		log.Fatal(err)
		return
	}
	accountHTML.Execute(w, page)
}

/*
Handles POSTs to /sign_up.
Validates the form and creates the new account.
*/
func SignUpHandler(w http.ResponseWriter, req *http.Request) {
	req.ParseForm()
	log.Println(req.Form)
	err := SignUp(req.Form)
	// Returns ErrUsernameInUse, ErrEmailInUse, ErrPasswordMismatch, or nil.
	if err == ErrUsernameInUse {
		http.Redirect(w, req, "/?error=username", 302)
		return
	} else if err == ErrEmailInUse {
		http.Redirect(w, req, "/?error=email", 302)
		return
	} else if err == ErrPasswordMismatch {
		http.Redirect(w, req, "/?error=password", 302)
		return
	} else if err == ErrNoForm {
		http.Redirect(w, req, "/?error=form", 302)
		return
	}
	
	token := GetToken(req.Form["username"][0])
	// Give them the token
	http.SetCookie(w, &http.Cookie{Name: "auth", Value: *token.Value, Expires: time.Now().Add(365 * 24 * time.Hour)})
	
	// Redirect them to their new account page
	http.Redirect(w, req, "/account", 302)
}

/*
Handles POSTs to /login.
Authenticates the user and assigns a session token.
*/
func LoginHandler(w http.ResponseWriter, req *http.Request) {
	req.ParseForm()
	log.Println(req.Form)
	err := Login(req.Form)
	if err != nil {
		log.Println("Error with login")
		http.Redirect(w, req, "/?error=creds", 302)
		return
	}
	
	token := GetToken(req.Form["username"][0])
	// Give them the token
	log.Println("Giving cookie")
	http.SetCookie(w, &http.Cookie{Name: "auth", Value: *token.Value, Expires: time.Now().Add(365 * 24 * time.Hour)})
	
	// Redirect them to their account page
	log.Println("Redirecting to account")
	http.Redirect(w, req, "/account", 302)
}

/*
Handles POSTs to /logout.
Deletes the user's cookie in their browser.
Removes the session token from the tokens table.
Redirects the user to /
*/
func LogoutHandler(w http.ResponseWriter, req *http.Request) {
	log.Println("Attempting logout.")
	// Check that the user is logged in
	authCookie, err := req.Cookie("auth")
	if err != nil {
		http.Redirect(w, req, "/", 403)
		return
	}
	// Reset their auth cookie
	http.SetCookie(w, &http.Cookie{Name: "auth", Value: "0", Expires: time.Now()})
	// Don't really care about the error returned here
	account, _ := DB.RequestSessionOwner(authCookie.Value)
	if account != nil {
		token := &DB.Token{&authCookie.Value, account.Username}
		token.Remove()
	}
	// Send them home
	http.Redirect(w, req, "/", 302)
}

/*
Handles POSTs to /deactivate.
Authenticates the user, resets their session token, and deletes their account.
*/
func DeactivateHandler(w http.ResponseWriter, req *http.Request) {
	log.Println("Attempting deactivate.")
	// Check that the user is logged in
	authCookie, err := req.Cookie("auth")
	if err != nil {
		http.Redirect(w, req, "/", 403)
		return
	}
	// Reset their auth cookie
	http.SetCookie(w, &http.Cookie{Name: "auth", Value: "0", Expires: time.Now()})
	// Don't really care about the error returned here
	account, _ := DB.RequestSessionOwner(authCookie.Value)
	token := &DB.Token{&authCookie.Value, account.Username}
	token.Remove()
	// Delete their account
	account.Remove()
	// Send them home
	http.Redirect(w, req, "/", 302)
}

/*
Handles POSTs to /change_password.
Authenticates the user, validates the form, and updates the user's password.
*/
func PasswordHandler(w http.ResponseWriter, req *http.Request) {
	log.Println("Attempting password update.")
	// Check that the user's session is valid
	authCookie, err := req.Cookie("auth")
	if err != nil {
		http.Redirect(w, req, "/", 403)
		return
	}
	// Check the account exists
	account, err := DB.RequestSessionOwner(authCookie.Value)
	if err != nil {
		http.Redirect(w, req, "/", 403)
		return
	}
	// Check that the form is valid
	req.ParseForm()
	log.Println(req.Form)
	if req.Form["current-password"] == nil ||
		req.Form["new-password"] == nil ||
		req.Form["repeat-password"] == nil {
			log.Println("One of the form fields is nil")
			http.Redirect(w, req, "/settings", 302)
			return
	}
	// Check that their current password is correct
	err = bcrypt.CompareHashAndPassword([]byte(*account.PasswordHash), []byte(req.Form["current-password"][0]))
	if err != nil {
		http.Redirect(w, req, "/settings?error=current", 302)
		return
	}
	// Check that the new passwords are identical
	if req.Form["repeat-password"][0] != req.Form["new-password"][0] {
		http.Redirect(w, req, "/settings?error=mismatch", 302)
		return
	}
	// Create the new password hash
	passwordBytes := []byte(req.Form["new-password"][0])
	newHashBytes, err := bcrypt.GenerateFromPassword(passwordBytes, 0)
	hashString := string(newHashBytes)
	log.Println("Updating hash: " + hashString)
	account.PasswordHash = &hashString
	// Update their password hash
	account.Update()
	// Reload their settings page
	http.Redirect(w, req, "/settings?success=true", 302)
}

/*
Handles GETs to /settings.
*/
func SettingsHandler(w http.ResponseWriter, req *http.Request) {
	// Check that the user is logged in
	authCookie, err := req.Cookie("auth")
	if (err != nil || !LoggedIn(authCookie)) {
		http.Redirect(w, req, "/", 403)
		return
	}
	acct, _ := GetAccount(authCookie)
	settingsHTML, err := template.ParseFiles("templates/settings.tpl")
	if err != nil {
		log.Fatal(err)
		return
	}
	settingsHTML.Execute(w, acct)
}

/*
Handles GETs to /play.
Serves the play page with a songs panel and corresponding controls
if playing a playlist, or with just the metronome and play/pause/restart
if playing a single song.
*/
func PlayHandler(w http.ResponseWriter, req *http.Request) {
	// Check that the user is logged in
	authCookie, err := req.Cookie("auth")
	if (err != nil || !LoggedIn(authCookie)) {
		http.Redirect(w, req, "/", 403)
		return
	}
	// Get the user's account
	acct, err := GetAccount(authCookie)
	if err != nil {
		http.Redirect(w, req, "/", 403)
		return
	}
	// Parse the form
	req.ParseForm()
	if req.Form == nil ||
		req.Form["id"] == nil ||
		req.Form["id"][0] == "" {
			log.Println("User tried to play a song without submitting a form.")
			http.Redirect(w, req, "/account", 403)
			return
	}
	// Check the user owns the song or playlist
	var page PlayPage
	if strings.Contains(req.Form["id"][0], "pl-") {
		idString := strings.Trim(req.Form["id"][0], "pl-")
		id, err := strconv.Atoi(idString)
		if err != nil {
			// Malformed input
			log.Println("User submitted a malformed play id.")
			http.Redirect(w, req, "/account", 403)
			return
		}
		playlist, err := DB.RequestPlaylist(id)
		if err != nil {
			// Tried to play a playlist which doesn't exist
			http.Redirect(w, req, "/account", 403)
			return
		}
		if *playlist.Owner.Username != *acct.Username {
			// User doesn't own the playlist
			http.Redirect(w, req, "/account", 403)
			return
		}
		page.Playlist = playlist
	} else if strings.Contains(req.Form["id"][0], "so-") {
		idString := strings.Trim(req.Form["id"][0], "so-")
		id, err := strconv.Atoi(idString)
		if err != nil {
			// Malformed input
			log.Println("User submitted a malformed play id.")
			http.Redirect(w, req, "/account", 403)
			return
		}
		song, err := DB.RequestSong(id)
		if err != nil {
			// Tried to play a song which doesn't exist
			http.Redirect(w, req, "/account", 403)
			return
		}
		if *song.Owner.Username != *acct.Username {
			// User doesn't own the song
			http.Redirect(w, req, "/account", 403)
			return
		}
		page.Song = song
	} else {
		// Malformed input
		log.Println("User submitted a malformed play id.")
		http.Redirect(w, req, "/account", 403)
		return
	}
	page.Account = acct
	playHTML, err := template.ParseFiles("templates/play.tpl")
	if err != nil {
		log.Fatal(err)
		return
	}
	playHTML.Execute(w, page)
}

/*
Handles GETs to /playlist_edit.
Serves the playlist edit page with songs from the playlist to edit,
or with a blank playlist and songlist.
*/
func PlaylistEditHandler(w http.ResponseWriter, req *http.Request) {
	// Check that the user is logged in
	authCookie, err := req.Cookie("auth")
	if (err != nil || !LoggedIn(authCookie)) {
		http.Redirect(w, req, "/", 403)
		return
	}
	// Parse the ID from the form
	req.ParseForm()
	page := PlaylistEditPage{}
	// Assume no account error because they are logged in fine
	page.Account, _ = GetAccount(authCookie)
	// If there's no form, serve an empty page
	if req.Form["id"] != nil && req.Form["id"][0] != "new" {
		playlistId, err := strconv.Atoi(req.Form["id"][0])
		if err != nil {
			log.Println("User tried to edit a playlist with invalid id.")
		}
		page.Playlist, err = DB.RequestPlaylist(playlistId)
		if err != nil {
			log.Println("User tried to edit a playlist they don't own.")
		}
	} else {
		// Create a blank song for the user to edit
		title := "Playlist Title"
		var songs *[]DB.Song
		page.Playlist = &DB.Playlist{Title: &title, Owner: page.Account, Songs: songs}
		n := true
		page.New = &n
	}
	log.Println("page.Playlist.Songs in PlaylistEditHandler:")
	log.Println(page.Playlist.Songs)
	page.SongList = page.Account.Songs()
	playlistEditHTML, err := template.ParseFiles("templates/playlist_edit.tpl")
	if err != nil {
		log.Fatal(err)
		return
	}
	playlistEditHTML.Execute(w, page)
}

/*
Handles GETs to /song_edit.
Serves the song edit page with values from the song to edit,
or with default values if editing a new song.
*/
func SongEditHandler(w http.ResponseWriter, req *http.Request) {
	// Check that the user is logged in
	authCookie, err := req.Cookie("auth")
	if (err != nil || !LoggedIn(authCookie)) {
		http.Redirect(w, req, "/", 403)
		return
	}
	// Parse the ID from the form
	req.ParseForm()
	page := SongEditPage{}
	// Assume no account error because they are logged in fine
	page.Account, _ = GetAccount(authCookie)
	// If there's no form, serve an empty page
	if req.Form["id"] != nil && req.Form["id"][0] != "new" {
		songId, err := strconv.Atoi(req.Form["id"][0])
		if err != nil {
			log.Println("User tried to edit a song with invalid id.")
		}
		page.Song, err = DB.RequestSong(songId)
		if err != nil {
			log.Println("User tried to edit a song they don't own.")
		}
	} else {
		// Create a blank song for the user to edit
		title := "Song Title"
		bpm := 120
		upper := 4
		lower := 4
		duration := 120
		page.Song = &DB.Song{Title: &title, BPM: &bpm, Upper: &upper, Lower: &lower, Duration: &duration, Owner: page.Account}
	}
	songEditHTML, err := template.ParseFiles("templates/song_edit.tpl")
	if err != nil {
		log.Fatal(err)
		return
	}
	// Serve the page
	log.Println(page.Song.Duration)
	err = songEditHTML.Execute(w, page)
	if err != nil {
		log.Println(err)
	}
}

/*
Handles POSTs to /save_song.
*/
func SaveSongHandler(w http.ResponseWriter, req *http.Request) {
	log.Println("Saving song.")
	req.ParseForm()
	log.Println(req.Form)
	// Check that the user is logged in
	authCookie, err := req.Cookie("auth")
	if (err != nil || !LoggedIn(authCookie)) {
		http.Redirect(w, req, "/", 403)
		return
	}
	// Check the form is not nil
	if req.Form["title"] == nil ||
		req.Form["bpm"] == nil ||
		req.Form["upper"] == nil ||
		req.Form["lower"] == nil ||
		req.Form["advanced"] == nil ||
		req.Form["duration"] == nil ||
		req.Form["id"] == nil {
			log.Println("Some form part is nil")
			http.Redirect(w, req, "/account", 302)
			return
	}
	// Get the user's account
	acct, err := GetAccount(authCookie)
	if err != nil {
		log.Println("Error with GetAccount in Save Song")
		log.Println(err)
		return
	}
	// Figure out whether the song is new or not
	if req.Form["id"][0] == "new" {
		// Continue to create the "new" song
		var id *int
		newSong := &DB.Song{Id: id, Owner: acct}
		// Give the song a title
		if req.Form["title"][0] == "" {
			// Give the song an untitled title
			title := fmt.Sprintf("Untitled %d", *acct.NewUntitled())
			newSong.Title = &title
		} else {
			newSong.Title = &req.Form["title"][0]
		}
		if req.Form["advanced"][0] != "" {
			// Set BPM, Upper, Lower, and Duration to nil
			var n *int
			newSong.BPM = n
			newSong.Upper = n
			newSong.Lower = n
			newSong.Duration = n
			// Set the advanced
			newSong.Advanced = &req.Form["advanced"][0]
		} else {
			var n *string
			// Set Advanced to nil
			newSong.Advanced = n
			// Checking the validity of this is a mess
			// Should provide an error to the user and leave values as-is
			// Will be done client-side, maybe I shouldn't worry
			bpm, err := strconv.Atoi(req.Form["bpm"][0])
			if err != nil {
				log.Println("User tried editing a song with invalid bpm")
				http.Redirect(w, req, "/account", 302)
				return
			}
			newSong.BPM = &bpm
			upper, err := strconv.Atoi(req.Form["upper"][0])
			if err != nil {
				log.Println("User tried editing a song with invalid upper")
				http.Redirect(w, req, "/account", 302)
				return
			}
			newSong.Upper = &upper
			lower, err := strconv.Atoi(req.Form["lower"][0])
			if err != nil {
				log.Println("User tried editing a song with invalid lower")
				http.Redirect(w, req, "/account", 302)
				return
			}
			newSong.Lower = &lower
			duration, err := strconv.Atoi(req.Form["duration"][0])
			if err != nil {
				log.Println("User tried editing a song with invalid duration")
				http.Redirect(w, req, "/account", 302)
				return
			}
			newSong.Duration = &duration
		}
		// Store the new song in the database
		newSong.Store()
	} else if req.Form["id"][0] != "new" { // Not new
		// Validate the form Id
		songId, err := strconv.Atoi(req.Form["id"][0])
		if err != nil {
			log.Println("User tried editing a song with invalid Id")
			http.Redirect(w, req, "/account", 302)
			return
		}
		// Validate the song exists
		song, err := DB.RequestSong(songId)
		if err == DB.ErrNoSong {
			log.Println("User tried editing a song that doesn't exist.")
			http.Redirect(w, req, "/account", 302)
			return
		}
		if err == DB.ErrNoAccount {
			log.Println("User tried editing an orphaned song.")
			http.Redirect(w, req, "/account", 302)
			return
		}
		// Validate the user owns the song
		if *acct.Username != *song.Owner.Username {
			log.Println("User tried editing song they don't own.")
			http.Redirect(w, req, "/account", 302)
			return
		}
		// Craft the submitted DB.Song
		var submitted DB.Song
		if req.Form["advanced"][0] == "" {
			// If the submitted song is not an advanced song
			bpm, err := strconv.Atoi(req.Form["bpm"][0])
			if err != nil {
				log.Println("User tried editing a song with invalid bpm")
				http.Redirect(w, req, "/account", 302)
				return
			}
			submitted.BPM = &bpm
			upper, err := strconv.Atoi(req.Form["upper"][0])
			if err != nil {
				log.Println("User tried editing a song with invalid upper")
				http.Redirect(w, req, "/account", 302)
				return
			}
			submitted.Upper = &upper
			lower, err := strconv.Atoi(req.Form["lower"][0])
			if err != nil {
				log.Println("User tried editing a song with invalid lower")
				http.Redirect(w, req, "/account", 302)
				return
			}
			submitted.Lower = &lower
			duration, err := strconv.Atoi(req.Form["duration"][0])
			if err != nil {
				log.Println("User tried editing a song with invalid duration")
				http.Redirect(w, req, "/account", 302)
				return
			}
			submitted.Duration = &duration
		} else {
			// If the submitted song is an advanced song
			submitted.Advanced = &req.Form["advanced"][0]
		}
		if req.Form["title"][0] == "" {
			submitted.Title = song.Title
		} else {
			submitted.Title = &req.Form["title"][0]
		}
		submitted.Id = song.Id
		submitted.Owner = acct
		// Update the old song
		submitted.Update()
	} else {
		// Not == "new" but also not != "new"
		log.Println("Something terribly wrong happened in SaveSongHandler.")
	}
	
	// Reload /account
	http.Redirect(w, req, "/account", 302)
}

/*
Handles POSTs to /delete_song.
*/
func DeleteSongHandler(w http.ResponseWriter, req *http.Request) {
	log.Println("Deleting song.")
	req.ParseForm()
	// Check that the user is logged in
	authCookie, err := req.Cookie("auth")
	if (err != nil || !LoggedIn(authCookie)) {
		http.Redirect(w, req, "/", 403)
		return
	}
	// Check the form included a song Id
	if req.Form == nil {
		http.Redirect(w, req, "/account", 302)
		return
	}
	// Check the logged in user owns that song
	songId, err := strconv.Atoi(req.Form["id"][0])
	if err != nil {
		http.Redirect(w, req, "/account", 302)
		return
	}
	song, err := DB.RequestSong(songId)
	if err == DB.ErrNoSong {
		log.Println("User tried deleting a song that doesn't exist.")
		http.Redirect(w, req, "/account", 302)
		return
	}
	if err == DB.ErrNoAccount {
		log.Println("User tried deleting an orphaned song.")
		http.Redirect(w, req, "/account", 302)
		return
	}
	acct, err := GetAccount(authCookie)
	if err != nil {
		log.Println("Error with GetAccount in delete Song")
		log.Println(err)
		return
	}
	if *acct.Username != *song.Owner.Username {
		log.Println("User tried deleting song they don't own.")
		log.Println(*acct.Username + " vs " + *song.Owner.Username)
		http.Redirect(w, req, "/account", 302)
		return
	}
	// Delete the song
	song.Remove()
	// Reload /account
	http.Redirect(w, req, "/account", 302)
}

/*
Handles POSTs to /save_playlist.
*/
func SavePlaylistHandler(w http.ResponseWriter, req *http.Request) {
	log.Println("Saving playlist.")
	req.ParseForm()
	log.Println(req.Form)
	// Check that the user is logged in
	authCookie, err := req.Cookie("auth")
	if (err != nil || !LoggedIn(authCookie)) {
		http.Redirect(w, req, "/", 403)
		return
	}
	// Check the form is not nil
	if req.Form == nil ||
		req.Form["playlist_data"] == nil ||
		req.Form["playlist_data"][0] == "" {
			log.Println("save_playlist form is nil")
			http.Redirect(w, req, "/account", 302)
			return
	}
	// Get the user's account
	acct, err := GetAccount(authCookie)
	if err != nil {
		log.Println("Error with GetAccount in Save Playlist")
		log.Println(err)
		http.Redirect(w, req, "/", 403)
		return
	}
	// Unmarshal the json
	var playlist SavedPlaylist
	err = json.Unmarshal([]byte(req.Form["playlist_data"][0]), &playlist)
	if err != nil {
		log.Println(err)
	}
	log.Println(*playlist.Songs)
	// Convert a new playlist to an actual DB.Playlist, then Store().
	if *playlist.New {
		var save DB.Playlist
		save.Title = playlist.Title
		// loop through playlist Ids adding to save.Songs
		var songs []DB.Song
		for _, id := range(*playlist.Songs) {
			song, err := DB.RequestSong(id)
			if err != nil {
				log.Println("Error with RequestSong in Save Playlist")
				log.Println(err)
				http.Redirect(w, req, "/", 403)
				return
			}
			if *song.Owner.Username != *acct.Username {
				// Trying to add a song they don't own to a playlist
				log.Println("User tried adding a song they don't own to a playlist.")
				http.Redirect(w, req, "/", 403)
				return
			}
			songs = append(songs, *song)
		}
		save.Songs = &songs
		save.Owner = acct
		save.Store()
	} else if !*playlist.New {
		var save DB.Playlist
		// Check the playlist to be edited exists
		old, err := DB.RequestPlaylist(*playlist.Id)
		if err != nil {
			// Trying to add a song they don't own to a playlist
			log.Println("User tried editing a playlist that doesn't exist.")
			http.Redirect(w, req, "/", 403)
			return
		}
		// Check the playlist to be edited is owned by the user
		if *old.Owner.Username != *acct.Username {
			log.Println("User tried editing a playlist they do not own.")
			http.Redirect(w, req, "/", 403)
			return
		}
		// Begin crafting a DB.Playlist corresponding to the SavedPlaylist
		save.Title = playlist.Title
		save.Id = playlist.Id
		save.Owner = acct
		// loop through playlist Ids adding to save.Songs
		var songs []DB.Song
		for _, id := range(*playlist.Songs) {
			song, err := DB.RequestSong(id)
			if err != nil {
				log.Println("Error with RequestSong in Save Playlist")
				log.Println(err)
				http.Redirect(w, req, "/", 403)
				return
			}
			if *song.Owner.Username != *acct.Username {
				// Trying to add a song they don't own to a playlist
				log.Println("User tried adding a song they don't own to a playlist.")
				http.Redirect(w, req, "/", 403)
				return
			}
			songs = append(songs, *song)
		}
		save.Songs = &songs
		save.Update()
	} else {
		// Playlist is not new, playlist Id is nil
		// Don't know what to do with this playlist
		log.Println("User saved a malformed playlist")
		http.Redirect(w, req, "/", 403)
		return
	}
	
	// Redirect to the account page, OK.
	http.Redirect(w, req, "/account", 302)
}

/*
Handles POSTs to /delete_playlist.
*/
func DeletePlaylistHandler(w http.ResponseWriter, req *http.Request) {
	log.Println("Deleting playlist.")
	req.ParseForm()
	// Check that the user is logged in
	authCookie, err := req.Cookie("auth")
	if (err != nil || !LoggedIn(authCookie)) {
		http.Redirect(w, req, "/", 403)
		return
	}
	// Check the form included a playlist Id
	if req.Form == nil || req.Form["id"] == nil{
		http.Redirect(w, req, "/account", 302)
		return
	}
	acct, _ := GetAccount(authCookie)
	// Check the logged in user owns that playlist
	playlistId, err := strconv.Atoi(req.Form["id"][0])
	if err != nil {
		http.Redirect(w, req, "/account", 302)
		return
	}
	playlist, err := DB.RequestPlaylist(playlistId)
	if err == DB.ErrNoPlaylist {
		log.Println("User tried deleting a playlist that doesn't exist.")
		http.Redirect(w, req, "/account", 302)
		return
	}
	if err == DB.ErrNoAccount {
		log.Println("User tried deleting an orphaned playlist.")
		http.Redirect(w, req, "/account", 302)
		return
	}
	if *acct.Username != *playlist.Owner.Username {
		log.Println("User tried deleting playlist they don't own.")
		http.Redirect(w, req, "/account", 302)
		return
	}
	// Delete the playlist
	playlist.Remove()
	// Reload /account
	http.Redirect(w, req, "/account", 302)
}

/*
Generates a new session token.
*/
func GetToken(username string) *DB.Token {
	// Create a session token for the new user to log them in
	byteArray := make([]byte, 32)
	if _, err := io.ReadFull(rand.Reader, byteArray); err != nil {
		log.Fatal(err)
	}
	value := base64.URLEncoding.EncodeToString(byteArray)
	token := &DB.Token{&value, &username}
	token.Store()
	return token
}

/*
Wrapper for DB.RequestSessionOwner()
Returns DB.ErrNoSession or nil.
*/ 
func GetAccount(c *http.Cookie) (*DB.Account, error) {
	a, err := DB.RequestSessionOwner(c.Value)
	if err != nil {
		return nil, err
	}
	return a, nil
}

/*
Checks the forms submitted by the user.
Logs the user in if valid.
Called by LoginHandler().
Returns ErrInvalidCredentials or nil.
*/
func Login(form url.Values) error {
	log.Println("Logging in")
	
	if form["username"] == nil || form["password"] == nil {
		return ErrInvalidCredentials
	}
	attemptAccount, err := DB.RequestAccount(form["username"][0])
	if err != nil {
		return ErrInvalidCredentials
	}
	err = bcrypt.CompareHashAndPassword([]byte(*attemptAccount.PasswordHash), []byte(form["password"][0]))
	if err != nil {
		return ErrInvalidCredentials
	}
	return nil
}

/*
Checks the forms submitted by the user. 
Creates a new account if valid.
Called by SignUpHandler().
Returns ErrUsernameInUse, ErrEmailInUse, ErrPasswordMismatch, or nil.
*/
func SignUp(form url.Values) error {
	log.Println("Signing up")
	if form["username"] == nil ||
		form["username"][0] == "" ||
		form["password"] == nil ||
		form["password"][0] == "" ||
		form["confirm-password"] == nil ||
		form["confirm-password"][0] == "" ||
		form["email"] == nil ||
		form["email"][0] == "" {
		return ErrNoForm
	}
	// Check if username is in use
	if DB.UsernameInUse(form["username"][0]) {
		return ErrUsernameInUse
	}
	// Check if email is in use
	if DB.EmailInUse(form["email"][0]) {
		return ErrEmailInUse
	}
	// Check if passwords match
	if form["password"][0] != form["confirm-password"][0] {
		log.Println(form["password"][0] + " does not match " + form["confirm-password"][0])
		return ErrPasswordMismatch
	}
	
	// If the program has reached this point, go ahead and create the account.
	passwordBytes := []byte(form["password"][0])
	passwordHash, err := bcrypt.GenerateFromPassword(passwordBytes, 0)
	if err != nil {
		panic(err)
	}
	hashString := string(passwordHash)
	untitled := 0
	a := &DB.Account{&form["username"][0], &hashString, &form["email"][0], &untitled}
	a.Store()
	log.Println("Signed up " + form["username"][0])
	return nil
}

/*
Returns true if logged in, false if not logged in.
*/
func LoggedIn(c *http.Cookie) bool {
	token := c.Value
	_, err := DB.RequestSessionOwner(token)
	if err != nil {
		return false
	}
	return true
}