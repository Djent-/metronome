<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>
			Try it
		</title>

		<link rel="icon" href="favicon.ico">
		<link href="css/main.css" rel="stylesheet">	
		<script
			  src="https://code.jquery.com/jquery-3.1.1.min.js"
			  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
			  crossorigin="anonymous"></script>
		<script src="js/Play.js"></script>
		
		<!-- Bootstrap -->
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</head>

	<body>
		<!-- Creates the navbar at the top of the page -->
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/home">
							Metronome
					</a>
				</div> <!-- /.navbar-header -->
				
				<!-- These are the elements on the right hand side of the navbar -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li class="active">
							<a href="/try_it">Try it</a>
						</li>
						{{if .LoggedOut}}
						<div style="display:none;"></div>
						{{else}}
						<li>
							<a href="/account">{{.Account.Username}}</a>
						</li>
						<li>
							<a href="/settings">Settings</a>
						</li>
						<li>
							<a href="/logout">Logout</a>
						</li>
						{{end}}
					</ul>
				</div><!-- /.navbar-collapse -->
			</div> <!-- /.container-fluid -->
		</nav>

		<div class="container-fluid">
			<div class="col-lg-5">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">
							Controls
						</h3>
					</div> <!-- /.panel .panel-heading -- >

					<!-- Metronome -->
					<div class="panel-body">
						<!-- Metronome control buttons. -->
						<form class="form-horizontal">
							<div class="form-group">
								<label for="bpm" class="col-sm-4 col-lg-4 control-label">
										BPM:
								</label>
								<div class="col-sm-6">
										<input class="form-control track-change" id="beats-per-min" type="number" name="bpm" value="120" min="60" max="220">
								</div>
								<div class="col-lg-12 col-sm-6" id="messageBPM">

								</div>
							</div>

							<div class="form-group">
								<label for="time-sig-upper" class="col-sm-4 col-lg-4 control-label">
										Time Signature:
								</label>
								<div class="row col-sm-6">
									<div class="col-sm-6">
										<input class="form-control track-change" id="time-sig-upper" type="number" name="time-sig-upper" value="4" min="1" max="32">
									</div>
									
									<div class="col-sm-6">
										<input class="form-control track-change" id="time-sig-lower" type="number" name="time-sig-lower" value="4" min="1" max="32">
									</div>
								</div>
								<div class="col-lg-12 col-sm-6" id="messageTsig">
									
								</div>
							</div>
							
							<div class="form-group">
								<label for="play-pause" class="col-sm-4 col-lg-4 control-label">
									
								</label>
								<div class="col-lg-4">
									<button type="button" id="play-pause" class="btn btn-default">
									Play
								</button>
								</div>
							</div>
						</form>
					</div> <!-- /.panel-body -->
				</div> <!-- /.panel .panel-default -->
			</div>
			
			<div class="col-lg-7">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">
							Metronome
						</h3>
					</div> <!-- /.panel .panel-heading -- >

					<!-- Metronome -->
					<div class="panel-body">
						<!-- The actual canvas being drawn on. -->
						<div id="metronome-container">
							<canvas id="fake" class="playlist-metronome"></canvas>
						</div>
					</div> <!-- /.panel-body -->
				</div> <!-- /.panel .panel-default -->
				
				<script src="js/PlayPage.js"></script>
			</div>
		</div>
	</body>
<html>	
