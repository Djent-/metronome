<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    
		<title>
			Metronome
		</title>
		<link rel="icon" href="favicon.ico">
		<link href="css/main.css" rel="stylesheet">
		<script
			  src="https://code.jquery.com/jquery-3.1.1.min.js"
			  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
			  crossorigin="anonymous"></script>
		<script src="js/Play.js"></script>
		
		<!-- Bootstrap -->
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	
	<body>
		<!-- Creates the navbar at the top of the page -->
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/home">
							Metronome
					</a>
				</div>
				
				<!-- These are the elements on the right hand side of the navbar -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li class="active">
							<a href="/try_it">Play<span class="sr-only">(current)</span></a>
						</li>
						<li>
							<a href="/account">{{.Account.Username}}</a>
						</li>
						<li>
							<a href="/settings">Settings</a>
						</li>
						<li>
							<a href="/logout">Logout</a>
						</li>
					</ul>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>
		
		<!-- The main body of the body -->
		<div class="container-fluid">
			{{if .Playlist}}
			<div class="col-lg-4 col-md-4 col-sm-12">
				<div class="panel panel-default">
					<div class="panel-heading" id="playlist-heading">
						<h3 class="panel-title text-center">
							{{.Playlist.Title}}
						</h3>
					</div>
				
					<div class="panel-body">
						<div class="list-group" id="playlist-menu">
							{{range .Playlist.Songs}}
							<a href="#" class="list-group-item">
								<h4 class="list-group-item-heading">
									{{.Title}}
								</h4>
								<p class="list-group-item-text">
									{{if .Duration}}{{.Duration}}{{else}}{{.Advanced}}{{end}}
								</p>
								{{if .Upper}}
								<input hidden class="advanced" value=""></input>
								<input hidden class="upper" value="{{.Upper}}"></input>
								<input hidden class="lower" value="{{.Lower}}"></input>
								<input hidden class="bpm" value="{{.BPM}}"></input>
								<input hidden class="duration" value="{{.Duration}}"></input>
								{{else}}
								<input hidden class="advanced" value="{{.Advanced}}"></input>
								{{end}}
							</a>
							{{end}}
						</div>
					</div>
				</div>
			</div> <!-- /.col-lg-4 .col-md-3 .col-sm-12-->
			{{end}}
			
			{{if .Playlist}}
			<div class="col-lg-8 col-md-8 col-sm-12">
			{{else}}
			<div class="col-lg-12 col-md-12 col-sm-12">
			{{end}}
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title text-center">
							Metronome
						</h3>
					</div> <!-- /.panel-heading -->
					<div class="panel-body">
						<!-- The actual canvas being drawn on. -->
						<div id="metronome-container">
							<canvas id="fake" class="playlist-metronome"></canvas>
						</div>
						
						<!-- The progress bar indicating the position within the song. -->
						<div class="progress">
							<div class="progress-bar progress-bar-striped active" 
								role="progressbar" aria-valuenow="0"
								aria-valuemin="0" aria-valuemax="100"
								style="width: 0%" id="progress">
									<span class="sr-only" id="progress-sr">
										0% Complete
									</span>
							</div> <!-- /.progress-bar -->
						</div> <!-- /.progress -->
						
						<!-- Metronome control buttons. -->
						<div class="btn-toolbar">
							<div class="btn-group" role="group" aria-label="...">
								{{if .Song}} <!-- This will need to be improved to support advanced songs. -->
								<button type="button" class="btn btn-default" id="current-ts" disabled>
									{{.Song.Upper}}/{{.Song.Lower}}
								</button>
								<input id="advanced" type="hidden" value="{{.Song.Advanced}}">
								<input id="duration" type="hidden" value="{{.Song.Duration}}">
								<input id="time-sig-upper" type="hidden" value="{{.Song.Upper}}">
								<input id="time-sig-lower" type="hidden" value="{{.Song.Lower}}">
								{{else}}
								<button type="button" class="btn btn-default" id="current-ts" disabled>
									4/4
								</button>
								<input id="advanced" type="hidden" value="">
								<input id="duration" type="hidden" value="120">
								<input id="time-sig-upper" type="hidden" value="4">
								<input id="time-sig-lower" type="hidden" value="4">
								{{end}}
							</div> <!-- /.btn-group -->
							
							<div class="btn-group" role="group" aria-label="...">
								{{if .Song}}
								<button type="button" id="restart-song" class="btn btn-default">
									<span class="glyphicon glyphicon-backward" aria-hidden=true></span>
								</button>
								{{else}}
								<button type="button" id="restart-playlist" class="btn btn-default">
									<span class="glyphicon glyphicon-fast-backward" aria-hidden=true></span>
								</button>
								<button type="button" id="restart-song" class="btn btn-default">
									<span class="glyphicon glyphicon-backward" aria-hidden=true></span>
								</button>
								{{end}}
								<button type="button" id="play-pause" class="btn btn-default">
									<span class="glyphicon glyphicon-play" aria-hidden=true></span>
								</button>
								{{if .Song}}
								{{else}}
								<button type="button" id="next-song" class="btn btn-default">
									<span class="glyphicon glyphicon-forward" aria-hidden=true></span>
								</button>
								{{end}}
							</div> <!-- /.btn-group -->
							
							<div class="btn-group" role="group" aria-label="...">
								{{if .Song}} <!-- This will need to be improved to support advanced songs. -->
								<button type="button" class="btn btn-default" id="current-bpm" disabled>
									{{.Song.BPM}} BPM
								</button>
								<input id="beats-per-min" type="hidden" value="{{.Song.BPM}}">
								{{else}}
								<button type="button" class="btn btn-default" id="current-bpm" disabled>
									120 BPM
								</button>
								<input id="beats-per-min" type="hidden" value="120">
								{{end}}
							</div> <!-- /.btn-group -->
						</div> <!-- /.btn-toolbar -->
					</div> <!-- /.panel-body -->
				</div> <!-- /.panel .panel-default -->
			</div> <!-- /.col-lg-8 .col-md-9 .col-sm-12-->

			<script src="js/PlayPage.js"></script>
		
		</div> <!-- /.container-fluid -->
	</body>
</html>