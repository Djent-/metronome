<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    
		<title>
			Account Settings
		</title>
		<link rel="icon" href="favicon.ico">
		<link href="css/main.css" rel="stylesheet">
		
		<script
			  src="https://code.jquery.com/jquery-3.1.1.min.js"
			  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
			  crossorigin="anonymous"></script>
		<script src="js/Settings.js"></script>
		
		<!-- Bootstrap -->
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	
	<body>
		<!-- Creates the navbar at the top of the page -->
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/home">
							Metronome
					</a>
				</div>
				
				<!-- These are the elements on the right hand side of the navbar -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="/try_it">Play</a>
						</li>
						<li>
							<a href="/account">{{.Username}}</a>
						</li>
						<!-- The Settings navigation item is active because this is the settings page -->
						<li class="active">
							<a href="/settings">Settings <span class="sr-only">(current)</span></a>
						</li>
						<li>
							<a href="/logout">Logout</a>
						</li>
					</ul>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>
		
		<!-- The main body of the body -->
		<div class="container-fluid">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">
							Change Password
						</h3>
					</div>
					<div class="panel-body">
						<form class="form-horizontal" action="/change_password" method="post">
							<!-- Error message -->
							<div class="col-lg-12" id="error"></div>
							<div class="form-group">
								<label for="inputPassword1" class="col-sm-4 control-label">
										Current password:
								</label>
								<div class="col-sm-8">
										<input class="form-control" id="inputPassword1" type="password" name="current-password">
								</div>
							</div>
							<div class="form-group">
								<label for="inputPassword2" class="col-sm-4 control-label">
										New password:
								</label>
								<div class="col-sm-8">
										<input class="form-control" id="inputPassword2" type="password" name="new-password">
								</div>
							</div>
							<div class="form-group">
								<label for="inputPassword3" class="col-sm-4 control-label">
										Repeat password:
								</label>
								<div class="col-sm-8">
										<input class="form-control" id="inputPassword3" type="password" name="repeat-password">
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-offset-4 col-sm-4">
									<button type="submit" class="btn btn-default">
										Submit
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">
							Deactivate Account
						</h3>
					</div>
					<div class="panel-body">
						<form action="deactivate" method="post">
							<div class="form-group">
								<label for="submit">
									By deactivating your account, you will lose access to all your songs and playlists.
								</label>
								<button class="btn btn-default" id="submit" type="submit">
									Deactivate
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>