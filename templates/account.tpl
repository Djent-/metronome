<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    
		<title>
			Account Page
		</title>
		<link rel="icon" href="favicon.ico">
		<link href="css/main.css" rel="stylesheet">
		
		<script
			  src="https://code.jquery.com/jquery-3.1.1.min.js"
			  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
			  crossorigin="anonymous"></script>
		
		<!-- Bootstrap -->
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<!-- Creates the navbar at the top of the page -->
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/home">
							Metronome
					</a>
				</div>
				
				<!-- These are the elements on the right hand side of the navbar -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="/try_it">Play</a>
						</li>
						<!-- The $USERNAME navigation item is active because this is the user's account page -->
						<li class="active">
							<a href="/account">{{.Account.Username}}<span class="sr-only">(current)</span></a>
						</li>
					
						<li>
							<a href="/settings">Settings</a>
						</li>
						<li>
							<a href="/logout">Logout</a>
						</li>
					</ul>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>

		<!-- Panel for displaying the user's list of Playlists -->
		<div class="container-fluid">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="panel panel-default">
					<div class="panel-heading clearfix">
					<!-- Button to add a new playlist redirects to playlist edit page -->
					<a href="/playlist_edit"><button type="button" class="btn btn-default btn-sm add-button">+</button></a>
					<h3 class = "panel-title text-center">
						Playlists
					</h3>
					</div>
					<div class="panel-body">
						<ul class="list-group">
							{{range .Playlists}}
							<li class="list-group-item clearfix">
								<form id="play-pl-{{.Id}}" action="/play" method="post">
									<!-- Play button links to play page -->
									<input type="hidden" name="id" value="pl-{{.Id}}">
									<input type="submit" class="btn btn-default play-button" value="Play">
								</form>
								<span class="title">{{.Title}}</span>
								<form id="delete-pl-{{.Id}}" action="/playlist_edit" method="post">
									<!-- Edit button links to edit page -->
									<input type="hidden" name="id" value="{{.Id}}">
									<input type="submit" class="btn btn-default edit-button" value="Edit">
								</form>
								<!-- Delete button to delete playlist -->
								<form id="delete-pl-{{.Id}}" action="/delete_playlist" method="post">
									<input type="hidden" name="id" id="del-pl-id" value="{{.Id}}">
									<input type="submit" class="btn btn-default delete-button" value="Del">
								</form>
							</li>
							{{end}}
						</ul>
					</div>
				</div>
			</div>
		
		<!-- Panel for displaying the user's list of Songs -->
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="panel panel-default">
					<div class="panel-heading clearfix">
						<h3 class="panel-title text-center">
							<a href="/song_edit"><button type="button" class="btn btn-default btn-sm add-button">+</button></a>
							Songs
						</h3>
					</div>
					<div class="panel-body">
						<ul class="list-group">
							{{range .Songs}}
							<li class="list-group-item clearfix">
								<form id="play-sg-{{.Id}}" action="/play" method="post">
									<!-- Play button links to play page -->
									<input type="hidden" name="id" value="so-{{.Id}}">
									<input type="submit" class="btn btn-default play-button" value="Play">
								</form>
								<span class="title">{{.Title}}</span>
								<form id="edit-sg-{{.Id}}" action="/song_edit" method="post">
									<!-- Edit button links to song_edit page -->
									<input type="hidden" name="id" value="{{.Id}}">
									<input type="submit" class="btn btn-default edit-button" value="Edit">
								</form>
								<!-- Delete button deletes song -->
								<form id="delete-sg-{{.Id}}" action="/delete_song" method="post">
									<input type="hidden" name="id" value="{{.Id}}">
									<input type="submit" class="btn btn-default delete-button" value="Del">
								</form>
							</li>
							{{end}}
						</ul>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
