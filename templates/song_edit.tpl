<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    
		<title>
			Song Edit Page
		</title>
		<link rel="icon" href="favicon.ico">
		<link href="css/main.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<!-- jQuery -->
		<script
			  src="https://code.jquery.com/jquery-3.1.1.min.js"
			  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
			  crossorigin="anonymous"></script>

		<!-- Bootstrap -->
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</head>
	<body>
		<!-- Creates the navbar at the top of the page -->
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/home">
							Metronome
					</a>
				</div>
				
				<!-- These are the elements on the right hand side of the navbar -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="/try_it">Play</a>
						</li>
						<li>
							<a href="/account">{{.Account.Username}}</a>
						</li>
						<li>
							<a href="/settings">Settings</a>
						</li>
						<li>
							<a href="/logout">Logout</a>
						</li>
					</ul>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>
		<!-- Panel for displaying general options for editing/creating a song -->
		<div class="container-fluid">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title text-center">
							Edit Song
						</h3>
					</div>
					<div class="panel-body">
						<form class="form-horizontal" action="/save_song" method="post">
							<div class="form-group">
								<label for="song-title" class="col-sm-4 control-label">
										Song Title:
								</label>
								<div class="col-sm-6">
										<input class="form-control" id="song-title" type="text" name="title" 
											placeholder={{if .Song.Title}}"{{.Song.Title}}"{{else}}"Song Title"{{end}}>
								</div>
							</div> 
							
							<div class="form-group">
								<label for="song-duration" class="col-sm-4 control-label">
										Measures:
								</label>
								<div class="col-sm-6">
										<input class="form-control" id="song-duration" type="number" name="duration"
											placeholder={{if .Song.Duration}}"{{.Song.Duration}}"{{else}}"70"{{end}}>
								</div>
							</div>

							<div class="form-group">
								<label for="beats-per-min" class="col-sm-4 control-label">
										BPM:
								</label>
								<div class="col-sm-6">
										<input class="form-control" id="beats-per-min" type="number" name="bpm"
											placeholder={{if .Song.BPM}}"{{.Song.BPM}}"{{else}}"100"{{end}}>
								</div>
							</div>

							<div class="form-group">
								<label for="time-sig-upper" class="col-sm-4 control-label">
										Time Signature:
								</label>
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
										<input class="form-control" id="time-sig-upper" type="number" name="upper"
											placeholder={{if .Song.Upper}}"{{.Song.Upper}}"{{else}}"4"{{end}}>
								</div>
								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
										<input class="form-control" id="time-sig-lower" type="number" name="lower"
											placeholder={{if .Song.Lower}}"{{.Song.Lower}}"{{else}}"4"{{end}}>
								</div>
							</div>
							<input type="hidden" name="id"
								value={{if .Song.Id}}"{{.Song.Id}}"{{else}}"new"{{end}}>
							<input type="hidden" name="advanced"
								value={{if .Song.Advanced}}"{{.Song.Advanced}}"{{else}}""{{end}}>
							<div class="form-group">
								<div class="col-sm-offset-4 col-sm-4">
									<button type="submit" class="btn btn-default">
										Save Song
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<!-- Panel for displaying advanced options for editing/creating a song -->
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="panel panel-default">
					<div class="panel-heading clearfix">
						<h3 class="panel-title text-center">
							<button type="button" class="btn btn-default btn-sm info-button" data-toggle="modal" data-target="#info-modal">
							?
						</button>
						<!-- Modal display box for user help -->
						<div class="modal fade" id="info-modal">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<h3>Advanced Settings</h3>
									</div>
									<div class="modal-body">
										<p>Advanced syntax should be in the form "(v bpm x/y z measures)(a bpm b/c d measures)... etc."</p>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
							Advanced Settings
						</h3>
					</div>
					<div class="panel-body">
						<form class="form-horizontal" action="/save_song" method="post">
							<div class="form-group">
								<label for="song-title" class="col-sm-4 control-label">
										Song Title:
								</label>
								<div class="col-sm-6">
										<input class="form-control" id="song-title" type="text" name="title" 
											{{if .Song.Title}} placeholder="{{ .Song.Title }}" {{else}} placeholder="Song Title" {{end}} >
								</div>
							</div> 
							
							<input type="hidden" name="bpm" 
								value={{if .Song.BPM}}"{{.Song.BPM}}"{{else}}""{{end}}>
							<input type="hidden" name="upper" 
								value={{if .Song.Upper}}"{{.Song.Upper}}"{{else}}""{{end}}>
							<input type="hidden" name="lower" 
								value={{if .Song.Lower}}"{{.Song.Lower}}"{{else}}""{{end}}>
							<input type="hidden" name="duration" 
								value={{if .Song.Duration}}"{{.Song.Duration}}"{{else}}""{{end}}>
							<input type="hidden" name="id" 
								value={{if .Song.Id}}"{{.Song.Id}}"{{else}}"new"{{end}}>
							
							<div class="form-group">
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-24">
									<label for="advanced-settings">Advanced Settings:</label>
									{{if .Song.Advanced}}
									<textarea class="form-control" rows="7" id="advanced-settings" name="advanced">{{ .Song.Advanced }}</textarea>
									{{else}}
									<textarea class="form-control" rows="7" id="advanced-settings" name="advanced" placeholder="Your code here..."></textarea>
									{{end}}
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-4">
									<button type="submit" class="btn btn-default">
										Save Song
									</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>