<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    
		<title>
			Playlist Edit Page
		</title>
		<link rel="icon" href="favicon.ico">
		<link href="css/main.css" rel="stylesheet">

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<!-- jQuery -->
		<script
			src="https://code.jquery.com/jquery-3.1.1.min.js"
			integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
			crossorigin="anonymous"></script>

		<script
		src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
		integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
		crossorigin="anonymous"></script>

		<script src="js/PlaylistEdit.js"></script>

		<!-- Bootstrap -->
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	</head>
	<body>
		<!-- Creates the navbar at the top of the page -->
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="/home">
							Metronome
					</a>
				</div>
				
				<!-- These are the elements on the right hand side of the navbar -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="/try_it">Play</a>
						</li>
						<li>
							<a href="/account">{{.Account.Username}}</a>
						</li>
						<li>
							<a href="/settings">Settings</a>
						</li>
						<li>
							<a href="/logout">Logout</a>
						</li>
					</ul>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container-fluid -->
		</nav>
		
		<!-- Panel for displaying the current Playlist -->
		<div class="container-fluid">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="panel panel-default">
					<div class="panel-heading clearfix">
					<div id="warning" style="display:none; color:red;">
						Playlist title cannot be empty.
					</div>
					<h3 class = "panel-title text-center">
						<button type="button" class="btn btn-default btn-sm info-button" data-toggle="modal" data-target="#info-modal">
							<span class="glyphicon glyphicon-info-sign"></span>
						</button>
						<!-- Modal display box for user help -->
						<div class="modal fade" id="info-modal">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<h3>Edit Your Playlist</h3>
									</div>
									<div class="modal-body">
										<p>To change the order of your playlist, simply drag songs to the position you want. To add new songs, drag them in from your list of songs.</p>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
						<div class="panel-title">
							<form class="form-horizontal" action="editPlaylist" method="post">
								<div class="form-group">
									<label for="playlist-title" class="col-sm-3 control-label">
											Playlist Title:
									</label>
									<div class="col-sm-6">
										<input class="form-control" id="playlist-title" type="text" name="PlaylistTitle" placeholder="{{.Playlist.Title}}">
									</div>
								</div> 
							</form>
						</div>
					</h3>
					</div>
					<!-- List of songs currently in the playlist. -->
					<div class="panel-body">
						<div class="list-group" id="playlist-list">
							{{if .Playlist.Songs}}
								<input hidden id="playlist-id" value="{{.Playlist.Id}}"></input>
								{{range .Playlist.Songs}}
								<a href="#" class="list-group-item song-item">
									<h4 class="list-group-item-heading">
										{{.Title}}
											<div class="btn btn-sm btn-default pull-right remove-song">
												<span class="glyphicon glyphicon-transfer"></span>
											</div>
									</h4>
									<p class="list-group-item-text">
										{{if .Duration}}{{.Duration}}{{else}}{{.Advanced}}{{end}}
									</p>
									<div class="song-id" style="display: none;">
										{{.Id}}
									</div>
								</a>
								{{end}}
							{{end}}
						</div>
					</div>
				</div>
				<!-- Button to submit playlist to /save_playlist -->
				<form class="form-horizontal" id="playlist-form" action="/save_playlist" method="post">
					<div class="form-group">
						<div class="col-sm-4">
							{{if .Playlist.Id}}
							<input hidden id="playlist-id" name="playlist-id" type="text" value="{{.Playlist.Id}}"></input>
							{{end}}
							<input hidden id="is_new" name="is_new" type="text" value="{{.New}}"></input>
							<input hidden id="playlist_data" name="playlist_data" type="text"></input>
						</div>
					</div>
				</form>
				<button id="save-button" onClick="submitForm();" class="btn btn-default">
					Save Playlist
				</button>
			</div>
			
			<!-- Panel for displaying the user's list of Songs -->
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title text-center">
							Your Songs
						</h3>
					</div>
					<div class="panel-body">
						<div class="list-group" id="song-list">
							{{range .SongList}}
							<a href="#" class="list-group-item">
								<h4 class="list-group-item-heading">
									{{.Title}}
									<button type="submit" class="btn btn-sm btn-default pull-right add-song">
										<span class="glyphicon glyphicon-transfer"></span>
									</button>	
								</h4>
								<p class="list-group-item-text">
									{{if .Duration}}{{.Duration}}{{else}}{{.Advanced}}{{end}}
								</p>
								<div class="song-id" style="display: none;">
									{{.Id}}
								</div>
							</a>
							{{end}}
						</div>
					</div>
				</div>
			</div>
		</div>

	</body>
	
</html>
