// main.go

package main

import (
	"log"
	"net/http"
	"os"
	"time"
	"html/template"
	"./server/account"
	"./server/db"
)

type TryItPage struct {
	LoggedOut *bool
	Account *DB.Account
}

func main() {
	// Setup the database handle
	DB.Database = DB.DatabaseSetup()
	defer DB.Database.Close()
	
	// Create http handlers for all the pages
	http.HandleFunc("/", HomePageHandler)
	http.HandleFunc("/sign_up", Account.SignUpHandler)
	http.HandleFunc("/login", Account.LoginHandler)
	http.HandleFunc("/logout", Account.LogoutHandler)
	http.HandleFunc("/deactivate", Account.DeactivateHandler)
	http.HandleFunc("/change_password", Account.PasswordHandler)
	http.HandleFunc("/account", Account.AccountPageHandler)
	http.HandleFunc("/try_it", TryItHandler)
	http.HandleFunc("/play", Account.PlayHandler)
	http.HandleFunc("/playlist_edit", Account.PlaylistEditHandler)
	http.HandleFunc("/song_edit", Account.SongEditHandler)
	http.HandleFunc("/delete_song", Account.DeleteSongHandler)
	http.HandleFunc("/save_song", Account.SaveSongHandler)
	http.HandleFunc("/save_playlist", Account.SavePlaylistHandler)
	http.HandleFunc("/delete_playlist", Account.DeletePlaylistHandler)
	http.HandleFunc("/settings", Account.SettingsHandler)
	http.HandleFunc("/js/", func(w http.ResponseWriter, req *http.Request) {
		log.Println("Serving: " + req.URL.Path[1:])
		if req.URL.Path[1:] == "js/Play.js" {
			http.ServeFile(w, req, "./js/Play.js")
		} else if req.URL.Path[1:] == "js/PlayPage.js" {
			http.ServeFile(w, req, "./js/PlayPage.js")
		} else if req.URL.Path[1:] == "js/Home.js" {
			http.ServeFile(w, req, "./js/Home.js")
		} else if req.URL.Path[1:] == "js/Settings.js" {
			http.ServeFile(w, req, "./js/Settings.js")
		} else if req.URL.Path[1:] == "js/PlaylistEdit.js" {
			http.ServeFile(w, req, "./js/PlaylistEdit.js")
		} else {
			log.Println("Did not serve: " + req.URL.Path[1:])
		}
	})
	http.HandleFunc("/assets/", func(w http.ResponseWriter, req *http.Request) {
		w.Header().Set("Cache-Control", "public, max-age=31536000")
		if req.URL.Path[1:] == "assets/majortick.wav" {
			log.Println("Serving: " + req.URL.Path[1:])
			major, _ := os.Open("assets/majortick.wav")
			http.ServeContent(w, req, "", time.Now(), major)
		} else if req.URL.Path[1:] == "assets/minortick.wav" {
			log.Println("Serving: " + req.URL.Path[1:])
			minor, _ := os.Open("assets/minortick.wav")
			http.ServeContent(w, req, "", time.Now(), minor)
		} else {
			log.Println("Did not serve: " + req.URL.Path[1:])
		}
	})
	http.HandleFunc("/fonts/", func(w http.ResponseWriter, req *http.Request) {
		if req.URL.Path[1:] == "fonts/Huggy_Bear.otf" {
			log.Println("Serving: " + req.URL.Path[1:])
			http.ServeFile(w, req, "./fonts/Huggy_Bear.otf")
		} else if req.URL.Path[1:] == "fonts/Huggy_Bear.ttf" {
			log.Println("Serving: " + req.URL.Path[1:])
			http.ServeFile(w, req, "./fonts/Huggy_Bear.ttf")
		} else {
			log.Println("Did not serve: " + req.URL.Path[1:])
		}
	})
	http.HandleFunc("/css/", func(w http.ResponseWriter, req *http.Request) {
		if req.URL.Path[1:] == "css/main.css" {
			log.Println("Serving: " + req.URL.Path[1:])
			http.ServeFile(w, req, "./css/main.css")
		} else {
			log.Println("Did not serve: " + req.URL.Path[1:])
		}
	})
	http.HandleFunc("/favicon.ico", func(w http.ResponseWriter, req *http.Request) {
		
	})

	// Start the server on port 8080
	log.Println("Starting listening server.")
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func HomePageHandler(w http.ResponseWriter, req *http.Request) {
	log.Println(req.URL.Path)
	// If the request has a valid login cookie, redirect to /account
	authCookie, err := req.Cookie("auth")
	if err == nil {
		if Account.LoggedIn(authCookie) {
			log.Println("User is logged in - redirecting to account page.")
			http.Redirect(w, req, "/account", 302)
			return
		}
	}
	
	homeHTML, err := template.ParseFiles("home.html")
	if err != nil {
		log.Fatal(err)
		return
	}
	homeHTML.Execute(w, nil)
}

func TryItHandler(w http.ResponseWriter, req *http.Request) {
	// If the request has a valid login cookie, include more in navbar
	authCookie, err := req.Cookie("auth")
	var page TryItPage
	if err == nil {
		if (Account.LoggedIn(authCookie)) {
			// User is logged in, include more in navbar
			page.Account, err = Account.GetAccount(authCookie)
			if err != nil {
				log.Fatal(err)
				return
			}
			log.Println(*page.Account.Username + " is on the try it page.")
		} else {
			t := true
			page.LoggedOut = &t
		}
	} else {
		// Create a blank DB.Account
		t := true
		page.LoggedOut = &t
	}
	tryItHTML, err := template.ParseFiles("templates/try_it.tpl")
	if err != nil {
		log.Fatal(err)
		return
	}
	tryItHTML.Execute(w, page)
}
