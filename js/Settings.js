// Settings.js

$(document).ready(function(){

	var url = window.location.href;
	var current = "?error=current";
	var mismatch = "?error=mismatch";
	var success = "?success=true";
	if(url.includes(current)){
		document.getElementById("error").innerHTML = "Current password incorrect.";
	}

	if(url.includes(mismatch)){
		document.getElementById("error").innerHTML = "New password repeated incorrectly.";
	}
	
	if(url.includes(success)){
		document.getElementById("error").innerHTML = "Password updated.";
	}
});