// PlaylistEdit.js

/*
This function looks through element id playlist-list
and creates a json object representing the playlist
as has been edited by the user. The relevant data for
submission is playlist title and the ids of all songs
currently in the playlist.
*/
/*
Instructions:
Create a json object.
Set a title attribute to the playlist title.
Iterate over the elements in playlist-list.
	- Add each song id to a list of song ids.
Set input id playlist_data's value to the resulting json.
Return.
*/
function submitForm() {
	// Playlist data object
	var playlistData = {
		"New": false,
		"Id": 0,
		"Title": document.getElementById("playlist-title").value.trim(),
		"Songs":[]
	};
	
	// Check title is not blank
	var title = document.getElementById("playlist-title").value.trim();
	if (title == "") {
		warnTitle();
		return;
	}
	
	// Set playlistData.New
	var New = document.getElementById("is_new").value;
	if (New == "true") {
		playlistData.New = true;
	}
	
	// If the song is not new, set the Id
	var id = document.getElementById("playlist-id");
	if (id) {
		id = id.value.trim();
		console.log(id);
		playlistData.Id = parseInt(id);
	}
	
	// Add each song to the songs list
	$("#playlist-list").find(".song-id").each(function(){
		playlistData.Songs.push(parseInt($(this).html().trim()));
	});
	
	// Set the hidden input to the object JSON
	document.getElementById("playlist_data").value = JSON.stringify(playlistData);
	
	// Submit the form
	document.getElementById("playlist-form").submit();
};

// Adds a warning message if the playlist title is blank
function warnTitle() {
	console.log("Setting warning");
	document.getElementById("warning").style = "color: red;";
}

/*
This function makes the playlist-list sortable so the user is able to drag
songs into the order they want
*/
$(function () {
    $("#playlist-list").sortable({
        update: function (event, ui) {
            var order = $(this).sortable('serialize');
            console.log(order);
        }
    })
    $("#playlist-list").disableSelection();
});


$(document).ready(function(){
/*
This function appends the list-group-item of the selected song to the playlist
when the add-song button is clicked
*/
	$("#song-list").on("click", ".add-song", function(){
		$("#playlist-list").append($(this).parent().parent());
		$(this).removeAttr("class", "add-song");
		//$(this).html('<span class="glyphicon glyphicon-remove"></span>');
		$(this).attr("class", "btn btn-sm btn-default pull-right remove-song");

	});
/*
This function moves the list-group-item of the selected song back to the user's 
list of songs wehn the remove-song button is clicked
*/
	$("#playlist-list").on("click", ".remove-song", function(){
		$("#song-list").prepend($(this).parent().parent());
		$(this).removeAttr("class", "remove-song");
		$(this).attr("class", "btn btn-sm btn-default pull-right add-song");
	});
});




