// PlayPage.js

var valuesChanged = true;
var index = 0;
var loaded = false;

// Initially set the first song's information
var m = document.getElementById("playlist-menu");
if (m) {
	// If the song is a basic song
	if (m.children[index].children[4]) {
		document.getElementById("duration").value = m.children[index].children[6].value;
		document.getElementById("time-sig-upper").value = m.children[index].children[3].value;
		document.getElementById("time-sig-lower").value = m.children[index].children[4].value;
		document.getElementById("beats-per-min").value = m.children[index].children[5].value;
		setCurrents(m.children[index].children[3].value, 
			m.children[index].children[4].value,
			m.children[index].children[5].value);
	} else {
		document.getElementById("advanced").value = m.children[index].children[2].value;
		state = advancedState(m.children[index].children[2].value)
		setCurrents(state.Upper, 
			state.Lower,
			state.BPM);
		document.getElementById("duration").value = state.Duration;
	}

	// Set the current song as active
	m.children[index].className = m.children[index].className + " active";
} else if (document.getElementById("advanced") == null) {
	// On the try it page
	console.log("Loading basic song, no playlist, or on Try It page.");
	
} else if (document.getElementById("advanced").value != "<nil>") {
		// Playing an advanced song, not in a playlist
		console.log("Loading advanced song, no playlist.");
		state = advancedState(document.getElementById("advanced").value)
		setCurrents(state.Upper, 
			state.Lower,
			state.BPM);
		document.getElementById("duration").value = state.Duration;
}

/*
This file is loaded by play.html.
It makes all the UI elements shrink to fit any window size.
*/
/*
// jQuery handler for when the document is ready
$(document).ready(handleReady);

// Called by the document ready handler
function handleReady() {
	
	// Create a handler for when the window is resized
	$(window).resize(function(){

		var VISIBLE = $(window).height();
		
		// The playlist list needs to have a scrollbar if it is longer than the window.
		var visibleHeight;
		if ($(document).height() < VISIBLE) {
			visibleHeight = VISIBLE;
		} else {
			visibleHeight = $(document).height();
		}
		visibleHeight = visibleHeight - $('#playlist-menu').offset().top;
		visibleHeight = visibleHeight - window.scrollMaxY;
		visibleHeight = visibleHeight - parseInt($('.panel').css('margin-bottom'));
		visibleHeight = visibleHeight - parseInt($('.panel-body').css('padding-bottom'));
		
		// Taking into account the stuff in the playlist panel
		var playlistMargin = parseInt($('#playlist-menu').css('margin-bottom'));
		
		// Taking into account the stuff in the metronome panel
		var canvasExtra = parseInt($('.progress').css('margin-top'));
		canvasExtra += parseInt($('.progress').css('margin-bottom'));
		canvasExtra += $('.progress').height();
		canvasExtra += $('#controls').outerHeight();
		
		// Change the height of #playlist-menu
		$('#playlist-menu').css({
			'max-height': visibleHeight - playlistMargin - 1
		});
		// Change the height of #metronome
		$('.playlist-metronome').css({
			'height': visibleHeight - canvasExtra - 1
		});
		
	});
	
	// Just call the resize handler to get it to run the first time
	$(window).resize();
}
*/

// Click handler for the play-pause button
document.getElementById("play-pause").addEventListener("click", function(e) {
	if (!loaded) {
		try {
			lsource.noteOn(1);
		} catch(e) {
			lsource.start();
		}
		loaded = true;
	}
	// Toggle the play-pause button to become the opposite
	// and play/pause the metronome.
	if ($("#play-pause").html().trim() === "Pause") {
		$("#play-pause").html("Play");
		metronome.pause();
	} else if ($("#play-pause").html().trim() === "Play") {
		console.log("Playing Try It beat.");
		$("#play-pause").html("Pause");
		if (valuesChanged) {
			var upper = document.getElementById("time-sig-upper").value;
			var lower = document.getElementById("time-sig-lower").value;
			var bpm = document.getElementById("beats-per-min").value;
			metronome = new Metronome(upper,lower,bpm);
			valuesChanged = false;
		}
		metronome.start();
	}
	if ($("#play-pause").children().attr("class") === "glyphicon glyphicon-pause") {
		$("#play-pause").children().removeClass("glyphicon-pause").addClass("glyphicon-play");
		metronome.pause();
	} else if ($("#play-pause").children().attr("class") === "glyphicon glyphicon-play") {
		$("#play-pause").children().removeClass("glyphicon-play").addClass("glyphicon-pause");
		if (valuesChanged) {
			var duration = document.getElementById("duration").value;
			var upper = document.getElementById("time-sig-upper").value;
			var lower = document.getElementById("time-sig-lower").value;
			var bpm = document.getElementById("beats-per-min").value;
			// If this page is playing a playlist
			if (document.getElementById("playlist-menu")) {
				if (document.getElementById("playlist-menu").children[index].children[3]) {
					metronome = new Metronome(upper,lower,bpm,duration,switchSong,1);
				} else {
					console.log("Creating advanced metronome from play button!");
					switchState();
				}
			// If this page is playing a single song.
			} else {
				if (document.getElementById("advanced").value != "<nil>") {
					switchState();
				} else {
					metronome = new Metronome(upper,lower,bpm,duration);
				}
			}
			metronome.clear();
			metronome.draw(performance.now());
			valuesChanged = false;
		}
		metronome.start();
	}
});

// Click handling for the restart playlist button
$("#restart-playlist").click(function() {
	metronome.pause();
	switchSong(0);
});

// Click handling for the restart song button
$("#restart-song").click(function() {
	metronome.pause();
	switchSong(index);
});

// Click handling for the next song button
$("#next-song").click(function() {
	metronome.pause();
	console.log("");
	console.log("Switching to song " + (index + 1))
	switchSong(index+1);
});

// Change handler for the Try It page inputs.
$(".track-change").change(function() {
	valuesChanged = true;
});

// Click handler for the playlist songs class list-group-item
$(".list-group-item").click(function() {
	console.log(this);
	for (i = 0; i < document.getElementById("playlist-menu").children.length; i++) {
		if (document.getElementById("playlist-menu").children[i] == this) {
			metronome.pause();
			switchSong(i);
			return;
		}
	}
	console.log("Never found song.");
});

/*
This function parses in the state of an advanced song.
*/
function advancedState(syntax, index) {
	console.log("Advanced syntax: " + syntax)
	var re = /\((\d*) bpm (\d*)\/(\d*) (\d*) measures\)/my;
	re.lastIndex = index || 0;
	var matches = re.exec(syntax);
	
	// End of song
	if (!matches) {
		return;
	}
	console.log("Matched: " + matches[0] + " at " + re.lastIndex);
	
	// Set the return object
	var ret = {"BPM": matches[1], 
		"Upper": matches[2], 
		"Lower": matches[3], 
		"Duration": matches[4],
		"NextIndex": re.lastIndex};
	return ret;
}

/*
This function sets the values of the inactive buttons
on either side of the control buttons.
*/
function setCurrents(upper, lower, bpm) {
	console.log("In setCurrents: upper: " + upper + " lower: " + lower + " bpm: " + bpm);
	document.getElementById("current-ts").innerHTML = upper + "/" + lower;
	document.getElementById("current-bpm").innerHTML = bpm + " BPM";
}

/*
This function gets passed to the metronome constructor
when playing advanced songs. It gets the next state of
the advanced song and creates a metronome for it.
*/
function switchState(syntaxIndex) {
	var syntax = document.getElementById("advanced").value;
	var state = advancedState(syntax, syntaxIndex);
	// End of song
	if (!state) {
		metronome.pause();
		if (document.getElementById("playlist-menu")) {
			switchSong(index + 1);
		} else {
			switchState();
			if ($("#play-pause").children().attr("class") === "glyphicon glyphicon-pause") {
				$("#play-pause").children().removeClass("glyphicon-pause").addClass("glyphicon-play");
			}
			metronome.pause();
		}
		return;
	}
	metronome = {};
	metronome = new Metronome(state.Upper,
			state.Lower,
			state.BPM,
			state.Duration,
			switchState,
			state.NextIndex);
	setCurrents(state.Upper, state.Lower, state.BPM);
	metronome.updateProgress();
	if ($("#play-pause").children().attr("class") === "glyphicon glyphicon-pause") {
		metronome.start();
	}
}

/*
This function gets passed to the metronome constructor
on play pages in which there is a playlist present. It
switches between songs when the current song ends.
*/
function switchSong(playlistIndex) {
	console.log("");
	console.log("Switching song to " + playlistIndex);
	// Set the current index to the given index
	// Get the values from the element at index playlistIndex
	// in the list of songs in the playlist and load its values
	// into the respective hidden inputs.
	var menu = document.getElementById("playlist-menu");
	// Make sure they aren't switching to an invalid song index
	if (!menu) {
		switchState();
		return;
	}
	if (playlistIndex < 0) {
		return;
	}
	if (playlistIndex > menu.children.length - 1) {
		return;
	}
	// Set the current index to the new index
	index = playlistIndex;
	console.log("Grabbing new values.");
	// If the song is a basic song
	if (menu.children[index].children[4]) {
		var upper = menu.children[index].children[3].value;
		var lower = menu.children[index].children[4].value;
		var bpm = menu.children[index].children[5].value;
		var duration = menu.children[index].children[6].value;
		document.getElementById("advanced").value = "";
		document.getElementById("duration").value = duration;
		document.getElementById("time-sig-upper").value = upper;
		document.getElementById("time-sig-lower").value = lower;
		document.getElementById("beats-per-min").value = bpm;
	} else { // advanced song
		var advanced = menu.children[index].children[2].value;
		document.getElementById("advanced").value = advanced;
	}
	// Set all the songs in the playlist to not include the active class
	for (i = 0; i < menu.children.length; i++) {
		menu.children[i].className = menu.children[i].className.replace(/\bactive\b/, '');
	}
	// Set the current song as active
	menu.children[index].className = menu.children[index].className + " active";
	// If the current song is a basic song
	if (menu.children[index].children[3]) {
		// Set the inactive buttons on either side of the control buttons
		setCurrents(upper, lower, bpm);
		// Reload the metronome object
		metronome = {};
		metronome = new Metronome(upper,lower,bpm,duration,switchSong,index+1);
	} else { // advanced song
		switchState();
	}
	metronome.clear();
	metronome.draw(performance.now());
	metronome.updateProgress();
	if ($("#play-pause").children().attr("class") === "glyphicon glyphicon-pause") {
		metronome.start();
	}
}

// Create the real canvas the size of the fake canvas.
// Position it directly over the fake canvas.
var WIDTH = $('#fake').width(); // width of fake canvas
var HEIGHT = $('#fake').height(); // height of fake canvas
var canvasElement = document.createElement('canvas');
canvasElement.id = "metronome";
canvasElement.width = WIDTH;
canvasElement.height = HEIGHT;
// the new canvas should be directly on top of the fake canvas
canvasElement.style.positon = "absolute";
// add the new canvas to the metronome container
document.getElementById('metronome-container').appendChild(canvasElement);

// Create the new metronome object
var metronome = new Metronome(4,4,120,80);
metronome.clear();
metronome.draw(performance.now());