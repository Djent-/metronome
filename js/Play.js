// Play.js

window.AudioContext = window.AudioContext || window.webkitAudioContext;
var audContext = new AudioContext();
var majorBuffer = null;
var minorBuffer = null;
var blankBuffer = null;
var lsource = audContext.createBufferSource();
lsource.buffer = audContext.createBuffer(1, 1, 22050);
lsource.connect(audContext.destination);

/*
Metronome constructor.
Called from PlayPage.js.
*/
var Metronome = function(upper, lower, bpm, duration, next, arg) {
	this.MAXANGLE = 2 * Math.PI / 3;
	this.MINANGLE = Math.PI / 3;
	
	// This is error checking specific to the Try It page.
    this.messageTsig = document.getElementById("messageTsig") || document.createElement('p');
    this.messageBPM = document.getElementById("messageBPM") || document.createElement('p');
	this.beatsPerMeasure = upper || 4;
	if (this.beatsPerMeasure < 1 || this.beatsPerMeasure > 32) {
		this.messageTsig.innerHTML = "Please enter a value between 1 and 32. Using automatic value of 4."
		this.beatsPerMeasure = 4;
	}
	this.lower = lower || 4;
	if (this.lower < 1 || this.lower > 32) {
		this.messageTsig.innerHTML = "Please enter a value between 1 and 32. Using automatic value of 4."
		this.lower = 4;
	}
	this.bpm = bpm || 120;
	if (this.bpm < 60 || this.bpm > 220) {
		this.messageBPM.innerHTML = "Please enter a value between 60 and 220. Using automatic BPM of 120."
		this.bpm = 120;
	}

	this.millisecondsBetweenBeats = this.musicNotationToMS();
	console.log("Upper: "+upper+" Lower: "+lower+" BPM: "+bpm);
	this.currentBeat = 1;
	this.elapsed = 0;
	this.duration = duration || 50000; // Random large number
	this.majorTick = new Audio("assets/majortick.wav");
	this.minorTick = new Audio("assets/minortick.wav");
	// Metronome canvas controllers
	this.toPause = false;
	this.request = false;
	// Metronome canvas context
	this.context = document.getElementById("metronome").getContext("2d");
	// Initial values for metronome needle
	// Lower point on needle line
	this.x1 = WIDTH / 2;
	this.y1 = 9 * HEIGHT / 10;
	// Length of needle line
	this.length = 8 * HEIGHT / 10;
	// this.angle holds the current angle of the metronome needle.
	// It starts on the left side, slightly adjusted so that the
	// major tick gets played when it reaches the right side.
	this.angle = this.MAXANGLE - Math.PI / 200;
	this.direction = "right"; // direction the needle is turning
	this.timeSinceLastMinMax = performance.now();
	this.next = next;
	this.arg = arg;
	
	// Set up the audio context and the major and minor buffers
	var minreq = new XMLHttpRequest();
	var majreq = new XMLHttpRequest();
	majreq.open("GET", "/assets/majortick.wav", true)
	minreq.open("GET", "/assets/minortick.wav", true)
	majreq.responseType = 'arraybuffer';
	minreq.responseType = 'arraybuffer';
	majreq.onload = function() {
		console.log("Major request loaded.");
		audContext.decodeAudioData(majreq.response, function(buffer) {
			majorBuffer = buffer;
		});
	}
	minreq.onload = function() {
		console.log("Minor request loaded.");
		audContext.decodeAudioData(minreq.response, function(buffer) {
			minorBuffer = buffer;
		});
	}
	majreq.send();
	minreq.send();
};

/*
This function is called by the click handler for the
.play-pause buttons. It starts the metronome for the current
beat or song at the beginning, or where it last left off
if paused. Consider: changing function name to 'play'.
Called from the play button handler in PlayPage.js.
*/
Metronome.prototype.start = function() {
	this.toPause=false;
	this.request=true;
	this.timeSinceLastMinMax = performance.now();
	this.draw(performance.now());
};

/*
This function plays the audio of the major tick and the minor tick.
It also keeps track of the current beat in the measure. It is called
from Metronome.prototype.draw().
*/
Metronome.prototype.tick = function() {
	if (this.toPause) {
		console.log("Trying to tick, but told to pause. Returning.")
		return;
	}
	if (this.currentBeat == 1) {
		// This is the first beat in the measure - play the major tick
		/* Old and busted
		this.minorTick.pause();
		this.minorTick.currentTime = 0;
		this.majorTick.play();
		console.log(this.majorTick);
		console.log("Played: " + this.majorTick.played);
		console.log("Can play type: " + this.majorTick.canPlayType());
		*/
		// New hotness
		var source = audContext.createBufferSource();
		source.buffer = majorBuffer;
		source.connect(audContext.destination);
		try {
			source.noteOn(1);
		} catch(e) {
			source.start();
		}
	} else {
		// This is not the first beat in the measure - play the minor tick
		/*
		this.minorTick.pause();
		this.minorTick.currentTime = 0;
		this.majorTick.pause();
		this.majorTick.currentTime = 0;
		this.minorTick.play();
		console.log(this.minorTick);
		console.log("Can play type: " + this.minorTick.canPlayType());
		*/
		// New hotness
		var source = audContext.createBufferSource();
		source.buffer = minorBuffer;
		source.connect(audContext.destination);
		try {
			source.noteOn(1);
		} catch(e) {
			source.start();
		}
	}
	
	if (this.currentBeat + 1 > this.beatsPerMeasure) {
		this.currentBeat = 1;
	} else {
		this.currentBeat += 1;
	}
	
	if (this.duration) {
		// Increment the number of elapsed beats
		this.elapsed = this.elapsed + 1;
		// Update the progress bar
		this.updateProgress();
		// If it's the end of the song, pause the metronome and
		// call the callback
		if (this.elapsed == this.duration * this.beatsPerMeasure) {
			console.log("");
			console.log("Song ended.");
			this.pause();
			//console.log("this.next: " + this.next);
			if (this.next) {
				if (this.arg) {
					console.log("Calling this.next with " + this.arg);
					this.next(this.arg);
				} else {
					console.log("Calling this.next");
					this.next();
				}
			}
		}
	}
}

/*
This function is called by the click handler for the
.play-pause buttons.
Called from the pause button handler in PlayPage.js.
*/
Metronome.prototype.pause = function() {
	this.toPause=true;
};

/*
This function calculates the number of milliseconds
between beats, determined by the current time signature
and beats per minutes of the current beat/song.
*/
Metronome.prototype.musicNotationToMS = function() {
	var spb = 60.0 / this.bpm; // seconds per quarter note
	var mspb = spb * 1000; // milliseconds per quarter note
	var tickInterval = Math.round(mspb * (4 / this.lower)); // milliseconds per whatever note
	
	return tickInterval;
};

/*
This function clears the metronome canvas.
Called initially by PlayPage.js and by Play.js in Metronome.prototype.draw().
*/
Metronome.prototype.clear = function() {
	// Clear the canvas by drawing a white rectangle
	this.context.beginPath();
	this.context.fillRect(0,0,WIDTH,HEIGHT);
	this.context.fillStyle = "white";
	this.context.stroke();
}

/*
This function draws the non-functional, purely aesthetic metronome components.
Called from Metronome.prototype.draw().
*/
Metronome.prototype.aesthetics = function() {
	// Draw a border around the swinging area.
	
	var x2 = this.x1 + this.length * Math.cos(this.MAXANGLE);
	var y2 = this.y1 - this.length * Math.sin(this.MAXANGLE);
	
	var x3 = this.x1 + this.length * Math.cos(this.MINANGLE);
	var y3 = this.y1 - this.length * Math.sin(this.MINANGLE);
	
	this.context.beginPath();
	this.context.strokeStyle = "grey";
	this.context.lineWidth = 1;
	this.context.moveTo(x2, y2);
	this.context.lineTo(this.x1, this.y1);
	this.context.lineTo(x3, y3);
	this.context.stroke();
}

/*
This function draws the metronome on the canvas.
Called from PlayPage.js and Metronome.prototype.start().
*/
Metronome.prototype.draw = function(DOMHighResTimeStamp) {
	// Clear the canvas.
	this.clear();
	
	// Draw the aesthetics.
	this.aesthetics();
	
	// Calculates the expected time between ticks, and corrects for the lag by
	// shortening the time until the next tick.
	// TODO: account for faster-than-expected ticks.
	if (DOMHighResTimeStamp - this.timeSinceLastMinMax >= this.millisecondsBetweenBeats) {
		// Send the needle to where it's supposed to be
		console.log("Lag detected: ");
		console.log("DOMHighResTimeStamp - this.timeSinceLastMinMax: " + (DOMHighResTimeStamp - this.timeSinceLastMinMax));
		console.log("this.millisecondsBetweenBeats: " + this.millisecondsBetweenBeats);
		if (this.direction == "right") {
			this.angle = this.MINANGLE;
		} else if (this.direction == "left") {
			this.angle = this.MAXANGLE;
		}
		// Make up for missed milliseconds
		this.timeSinceLastMinMax = performance.now() - (DOMHighResTimeStamp - this.timeSinceLastMinMax - this.millisecondsBetweenBeats);
	}
	
	// Calculate location of needle endpoint
	var x2 = this.x1 + this.length * Math.cos(this.angle);
	var y2 = this.y1 - this.length * Math.sin(this.angle);
	
	// Draw the needle
	this.context.beginPath();
	this.context.strokeStyle = "black";
	this.context.lineWidth = 2;
	this.context.moveTo(this.x1,this.y1);
	this.context.lineTo(x2,y2);
	this.context.stroke();
	
	// Update direction the needle is travelling,
	// and stop requesting frames if paused.
	
	// If the needle is tilting right, and has reached the minimum
	// angle it is allowed to be.
	if ((this.angle <= this.MINANGLE) && this.direction == "right") {
		this.direction = "left";
		if (this.toPause) {
			this.request = false;
			return;
		} else {
			this.timeSinceLastMinMax = performance.now();
			this.tick();
		}
	// If the needle is tilting left and has reached the maximum
	// angle it is allowed to be.
	} else if ((this.angle >= this.MAXANGLE) && this.direction == "left") {
		this.direction = "right";
		if (this.toPause) {
			this.request = false;
			return;
		} else {
			this.timeSinceLastMinMax = performance.now();
			this.tick();
		}
	}
	
	// Calculate the angle based on the time since the last known min/max angle
	if (this.direction == "left") {
		this.angle = (this.MINANGLE) + this.MINANGLE * (DOMHighResTimeStamp - this.timeSinceLastMinMax) / this.millisecondsBetweenBeats;
	} else if (this.direction == "right") {
		this.angle = (this.MAXANGLE) - this.MINANGLE * (DOMHighResTimeStamp - this.timeSinceLastMinMax) / this.millisecondsBetweenBeats;
	}
	
	// Request the animation frame if not paused
	if (this.request) {
		var self = this;
		window.requestAnimationFrame(function(DOMHighResTimeStamp) {
				self.draw(DOMHighResTimeStamp);
			}
		);
	}
}

/*
This function updates the progress bar below the metronome.
Called from Metronome.prototype.tick().
*/
Metronome.prototype.updateProgress = function() {
	var percentage;
	if (this.elapsed == 0) {
		percentage = 0;
	} else {
		percentage = Math.floor((this.elapsed / (this.duration * this.beatsPerMeasure)) * 100);
		console.log(percentage);
	}
	// Update div id="progress"
	$("#progress").attr({
		"aria-valuenow" : percentage,
		"style" : "width:" + percentage + "%"
	})
	// Update div id="progress-sr"
	$("#progress-sr").innerHTML = percentage + "% Complete";
}
